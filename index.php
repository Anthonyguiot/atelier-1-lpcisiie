<?php

require_once 'conf/autoload.php';
if(!isset($_SESSION['id']))
{
  session_start();
}


// une instance d'analiseur de requette 
$http = new \utils\HttpRequest();

//print_r($http);
$path = '\mediathequeapp\control\\'.$http->control;

$control = new $path();
$control->dispatch($http);
// compléter ici 
