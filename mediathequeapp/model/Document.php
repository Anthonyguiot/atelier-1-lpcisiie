<?php

 /**
 *  La classe Page
 *
 *  La Classe Page  realise un Active Record sur la table page
 */
 
namespace mediathequeapp\model;

class Document {


	/**
	 *  Les attribut d'une page 
	 *  @access private
	 */
	private $ref, $type,$genre, $nom, $descriptif, $auteur, $etat, $image,$date_retour, $id_adherant,$date_sortie; 
	
	/**
	 *  Constructeur de Page
	 *
	 *  fabrique une nouvelle categorie vide
	 */
	public function __construct() {
		// rien à faire
}


	/**
	 *  Magic pour imprimer
	 *
	 *  Fonction Magic retournant une chaine de caracteres imprimable
	 *  pour imprimer facilement une page pour la correction derreurs 
	 *
	 *  @return String
	 */
	public function __toString() {
		return "[". __CLASS__ . "] [id : ". $this->id .
			"] [title : " . $this->title .
			"] [text : " . $this->text .
			"] [author : " . $this->author .
			"] [date : " . $this->date . "]";
	}

	/**
	 *   Getter generique
	 *
	 *   fonction d'acces aux attributs d'un objet.
	 *   Recoit en parametre le nom de l'attribut accede
	 *   et retourne sa valeur.
	 *  
	 *   @param String $attr_name attribute name 
	 *   @return mixed
	 */

	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
			return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new \Exception($emess);
	}
	 
	
	/**
	 *   Setter generique
	 *
	 *   fonction de modification des attributs d'un objet.
	 *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
	 *  Nouvel
	 *   @param String $attr_name attribute name 
	 *   @param mixed $attr_val attribute value
	 */
 
	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) 
			$this->$attr_name=$attr_val; 
		else{
			$emess = __CLASS__ . ": unknown member $attr_name (__set)";
			throw new \Exception($emess);
		}
	}

	/**
	 *   Finder sur un mot clef
	 *
	 *   Recherche dans le titre et le descriptif stoké dans la base
	 *	 de donnée des documents le mot clef passé en paramètre, 
	 *	 retourne un tableau d'objets
	 *  
	 *   @static
	 *   @return renvoie un tableu d'objet de type Document
	 */
	public static function findDocument($categorie, $genre, $keyword) {
		
		$res = array();
		
		$pdo = \utils\Connexion::getConnexion();
		//cat+genre
		//cat
		//genre
		if (($categorie!=null)&&($genre!=null)){
			$select = $pdo->prepare("SELECT * from documents WHERE `type`=:categorie AND `genre`=:genre ");
			$select->bindParam(':categorie', $categorie, \PDO::PARAM_STR);
			$select->bindParam(':genre', $genre, \PDO::PARAM_STR);
		}
		if (($categorie!=null)&&($genre==null)){
			$select = $pdo->prepare("SELECT * from documents WHERE `type`=:categorie");
			$select->bindParam(':categorie', $categorie, \PDO::PARAM_STR);
		}
		if (($categorie==null)&&($genre!=null)){
			$select = $pdo->prepare("SELECT * from documents WHERE `genre`=:genre");
			$select->bindParam(':genre', $genre, \PDO::PARAM_STR);
		}
		if (($categorie==null)&&($genre==null)){
			$select = $pdo->prepare("SELECT * from documents");
		}
		if (!$select->execute()){
			return false;
		};
	
		$result = $select->fetchAll();
		foreach ($result as $key => $value) {
			// ou stristr
			if ($keyword!=null) {
				if ( (strstr($result[$key]['nom'], $keyword) == TRUE) || (strstr($result[$key]['descriptif'], $keyword) == TRUE)){
					$o              = new Document();
					$o->ref         = $result[$key]['ref'];
					$o->type        = $result[$key]['type'];
					$o->genre       = $result[$key]['genre'];
					$o->nom         = $result[$key]['nom'];
					$o->descriptif  = $result[$key]['descriptif']; 
					$o->auteur      = $result[$key]['auteur'];
					$o->image       = $result[$key]['image'];
					$res[]          = $o;
					
				}
			}	else{
				$o              = new Document();
				$o->ref         = $result[$key]['ref'];
				$o->type        = $result[$key]['type'];
				$o->genre       = $result[$key]['genre'];
				$o->nom         = $result[$key]['nom'];
				$o->descriptif  = $result[$key]['descriptif']; 
				$o->auteur      = $result[$key]['auteur'];
				$o->image       = $result[$key]['image'];
				$res[]          = $o;
			}
		}
	
		return $res;   
	}
	 /**
	 *   Finder sur genre
	 *
	 *   Liste tout les genres
	 *   retourne un tableau de string
	 *  
	 *   @static
	 *   @return renvoie un tableau 
	 */
	
	public static function findGenre() {
		
		$pdo = \utils\Connexion::getConnexion();
		
		$query = $pdo->prepare("SELECT `genre` from documents");
		
		$res = array();

		if( !$query->execute() ){
			return false;
		}
		$result = $query->fetchAll() ;
		if (!$result){
			return false;
		}
		foreach ($result as $key => $value) {
			$res[] = $result[$key]['genre'];
		}
		
		return array_unique($res);   
	}
	
	 /**
	 *   Finder sur type
	 *
	 *   Liste tout les type
	 *   retourne un tableau de string
	 *  
	 *   @static
	 *   @return renvoie un tableau 
	 */
	
	public static function findType() {
		
		$pdo = \utils\Connexion::getConnexion();
		
		$query = $pdo->prepare("SELECT `type` from documents");
		
		$res = array();

		if( !$query->execute() ){
			return false;
		}
		$result = $query->fetchAll() ;
		if (!$result){
			return false;
		}
		foreach ($result as $key => $value) {
			$res[] = $result[$key]['type'];
		}
		
		return array_unique($res);   
	}

	/**
	*
	*	
	*	La fonction saveEtat effectue une insertion d'un média dans la BDD (table "sortie")
	*	@author : Osman
	*
	**/
	public static function saveEtat(){

		$refdoc = $_POST['idmedia'];
		$idadherant = 'OD10';
		$datesortie = '0000-00-00'; //"on verra après"
		$dateretour = '0000-00-00'; // "on verra après"
		$etat = $_POST['etat'];

	  	$pdo = \utils\Connexion::getConnexion();
    
  	  	$query = $pdo->prepare(
	      "INSERT into sortie (`ref_doc` , `id_adherant` , `date_sortie` , `date_retour` , `etat`) values ('$refdoc', '$idadherant', '$datesortie', '$dateretour', '$etat')"); 


		if (!empty($refdoc) and !empty($idadherant) and !empty($datesortie) and !empty($dateretour) and !empty($etat) and $query->execute()) {

			echo ('Document sauvegardé !');
		} 

		else {

			echo ("Certaines informations sont manquantes, veuillez réessayer !
			       <a href='formetat'><input type='submit' class='' value='Retour'/></a>
			     ");
		}

	}



	/**
	*
	*	
	*	La fonction updateEtat effectue une mise a jour dans la BDD (table "sortie"),
	*	si le document et deja dans la table sortie
	*	La date de retour passe alors a 0000-00-00
	*	
	*
	**/
	public static function updateEtat($refdoc, $etat){
		$date_sortie =  date(('Y-m-d'));
		$date_retour = '0000-00-00';

	  	$pdo = \utils\Connexion::getConnexion();
    
  	  	$query = $pdo->prepare( "UPDATE `sortie` SET `date_sortie`=:date_sortie,`date_retour`=:date_retour,`etat`=:etat WHERE `ref_doc`=:ref_doc"); 
		$query->bindParam(':ref_doc',$refdoc, \PDO::PARAM_INT );
		$query->bindParam(':date_sortie',$date_sortie, \PDO::PARAM_STR );
		$query->bindParam(':date_retour',$date_retour, \PDO::PARAM_STR );
		$query->bindParam(':etat',$etat, \PDO::PARAM_STR );

		if ($query->execute()){
			return TRUE;
		}	else{
			return FALSE;
		}
	}

	/**
	 *   Finder sur un document
	 *
	 *   Recherche dans la table documents le document correspondant
	 *	 à la référence placé en paramètre 
	 *   @author willy
	 *   @static
	 *   @return renvoie un tableu d'objet de type Document
	 */
	public static function findDocumentByRef($ref) {

    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("SELECT * from documents where ref=:ref");
    $query->bindParam(':ref',$ref, \PDO::PARAM_INT);

    if( !$query->execute())
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;

    $verifDispo =$pdo->prepare("SELECT etat, date_retour from sortie where ref_doc=:ref");
    $verifDispo->bindParam(':ref',$ref, \PDO::PARAM_INT);

    if( !$verifDispo->execute())
      return false;
    
    $d = $verifDispo->fetch(\PDO::FETCH_OBJ) ;
    
    $o = new Document();
    $o->ref     = $p->ref;
    $o->type  = $p->type;
    $o->nom   = $p->nom;
    $o->descriptif   = $p->descriptif;
    $o->auteur   = $p->auteur;
    $o->image   = $p->image;
    $o->etat   = $d->etat;
    $o->date_retour   = $d->date_retour;
    return $o;
  }
  /**
  *
  * 
  * La fonction recupererdocuments renvoie un objet de type document
  * en fonction de sa reference @author Anthony
  *
  **/
public static function recupererdocuments($ref) {
    $pdo = \utils\Connexion::getConnexion();
    $res = array();
    foreach ($ref as $key => $value) {
      $query =$pdo->prepare("SELECT * from documents where ref=:ref");
      $query->bindParam(':ref',$value, \PDO::PARAM_INT);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;

    $o = new Document();
    $o->ref         = $p->ref;
    $o->type        = $p->type;
    $o->genre       = $p->genre;
    $o->nom         = $p->nom;
    $o->descriptif  = $p->descriptif;
    $o->auteur      = $p->auteur;
    $o->image       = $p->image;
    $res[]          = $o;
   }
      return $res;
}

  /**
  *
  * 
  * La fonction recupererdocumentsadhrents
  *permet la recuperation des document en possession de l'adherents pour le retour des doc
  *  @author Anthony
  *
  **/
   public static function recupereradherents($id) {
    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("SELECT * from sortie where id_adherant=:id_adherant");
    $query->bindParam(':id_adherant',$id);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
    
    $o = new Document();
    $o->ref    = $p->ref_doc ;
    $o->id_adherant  = $p->id_adherant;
    $o->date_retour   = $p->date_retour;
    //essayer de passer en tableau

    return $o;
  }

  public static function recupererdocumentsadherents($id) {
    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("SELECT * from sortie where ref_doc=:ref_doc");
    $query->bindParam(':ref_doc',$id);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
    
    $o = new Document();
    $o->ref    = $p->ref_doc ;
    $o->id_adherant  = $p->id_adherant;
    $o->date_retour   = $p->date_retour;

    return $o;
  }

  /**
  *
  * 
  * La fonction sortiedocument
  *permet d'inserer des documents dans la table sortie
  *  @author Anthony
  *
  **/
  public function sortiedocuments($reference,$identifiant) {
    $pdo = \utils\Connexion::getConnexion();

    $query = $pdo->prepare(
        "INSERT into sortie (`ref_doc`, `id_adherant`, `date_sortie`,`date_retour`,`etat`) values (:ref_doc,:id_adherant,:date_sortie,:date_retour,:etat)"); 
    $date=date(('Y-m-d'));
    $calculdate=date('Y-m-d', strtotime('+15 days'));
    $emprunt="emprunté";
    $query->bindParam(':ref_doc', $reference, \PDO::PARAM_STR);
    $query->bindParam(':id_adherant', $identifiant,  \PDO::PARAM_STR);
    $query->bindParam(':date_sortie', $date,  \PDO::PARAM_STR);
    $query->bindParam(':date_retour', $calculdate, \PDO::PARAM_STR);
    $query->bindParam(':etat', $emprunt,  \PDO::PARAM_STR);
    if( $query->execute() ){
      $this->id_sortie = $pdo->LastInsertId();
      return $this->id_sortie;
    }    else{
      return -1;
    }
  }
   /**
  *
  * 
  * La fonction rendredocument
  *permet d'effacer des documents dans la table sortie
  *  @author Anthony
  *
  **/
   public function rendredocument($ref) {
    $pdo = \utils\Connexion::getConnexion();
    $query = $pdo->prepare("DELETE from sortie where ref_doc = :ref_doc");
    $query->bindParam(":ref_doc", $ref);
    return $query->execute(); 
  }
     public function supprimereservation($ref) {
    $pdo = \utils\Connexion::getConnexion();
    $query = $pdo->prepare("DELETE from reservation where id_doc = :ref_doc");
    $query->bindParam(":ref_doc", $ref);
    return $query->execute(); 
  }

  /**
  *
  * 
  * La fonction verifier
  *permet de verifier que le documents emprunter ne l'ai pas deja
  *  @author Anthony
  *
  **/
  public function verifier($ref) {
    $pdo = \utils\Connexion::getConnexion();
    $query = $pdo->prepare("SELECT * from sortie where ref_doc = :ref_doc");
    $query->bindParam(":ref_doc", $ref);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p){
      return TRUE;
    }else{
      return false;
    }
  }

    public function verifierreservation($ref_doc,$ref_adh) {
    $pdo = \utils\Connexion::getConnexion();
    //$query = $pdo->prepare("SELECT * from reservation where id_adherant=:id_adherant AND id_doc=:id_doc");
    $query = $pdo->prepare("SELECT * from reservation where id_doc=:id_doc");

    //$query->bindParam(":id_adherant", $ref_adh);
    $query->bindParam(":id_doc", $ref_doc);
 	if( !$query->execute() ){
      	return false;
 	}
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if ( $p){
      	//return TRUE;
      	return $p->id_adherant;

    }
    else{
    	return false;
    }
  }


  public function text() {
    $o = new Document();
    $o->ref    = "Document non disponible";
}
  /**
  *
  * 
  * Vérifie dans la table reservation si le document n'existe pas, si aucun documents
  *	n'est retourné, on verifie dans sortie si le livre est disponible ou emprunté et si l'un
  *	des deux cas est validé, alors on ajoute le document dans réservation et on ajoute 15 jours
  *	à la date de retour si le livre est déjà emprunté ou 15 jours à la date du jour
  *
  *  @author willy
  *
  **/
  public function reserverDocument($refDoc,$id_adherant) {
    $pdo = \utils\Connexion::getConnexion();
  	$verifReserve =$pdo->prepare("SELECT * from reservation where id_doc=:refDoc");
    $verifReserve->bindParam(':refDoc',$refDoc, \PDO::PARAM_INT);

    if($verifReserve->execute()){
    	$reserver = $verifReserve->fetch(\PDO::FETCH_OBJ);

    	if($reserver == ''){
    		$verifEmprunt =$pdo->prepare("SELECT etat, date_retour from sortie where ref_doc=:refDoc");
    		$verifEmprunt->bindParam(':refDoc',$refDoc, \PDO::PARAM_INT);

    		if($verifEmprunt->execute()){
    			$p = $verifEmprunt->fetch(\PDO::FETCH_OBJ);

    			if($p->etat == 'emprunté' || $p == null){
    				if($p->etat != nul){
							$calculdate = date("Y-m-d", strtotime($p->date_retour." +15 days"));
    				}
    				else{
    					$calculdate=date('Y-m-d', strtotime('+15 days'));
    				}
    				
    				$ajoutReserve =$pdo->prepare("INSERT INTO `reservation`(`id_adherant`, `id_doc`, `date_limite`) VALUES (:id_adherent,:refDoc,:calculdate)");
    				$ajoutReserve->bindParam(':id_adherent',$id_adherant, \PDO::PARAM_INT);
    				$ajoutReserve->bindParam(':calculdate',$calculdate, \PDO::PARAM_STR);
    				$ajoutReserve->bindParam(':refDoc',$refDoc, \PDO::PARAM_INT);

    				if($ajoutReserve->execute()){
    					$res = 'ok';
    				}
    			}
    			else{
    				$res = 'Ce livre ne peut pas être emprunté';
    			}
    		}
    	}
    	else{
    		$res = 'Ce livre est déjà réservé';
    	}
    }
    return $res;
  }
  	




	public static function ajoutDoc($type, $genre, $nom, $descriptif, $auteur, $image){

	  	$pdo = \utils\Connexion::getConnexion();

  	  	$query = $pdo->prepare("INSERT into documents (`type` , `genre` , `nom` , `descriptif` , `auteur` , `image`) values (:type, :genre, :nom, :descriptif, :auteur, :image)");

		$query->bindParam(":type", $type, \PDO::PARAM_STR);
		$query->bindParam(":genre", $genre, \PDO::PARAM_STR);
		$query->bindParam(":nom", $nom, \PDO::PARAM_STR);
		$query->bindParam(":descriptif", $descriptif, \PDO::PARAM_STR);
		$query->bindParam(":auteur", $auteur, \PDO::PARAM_STR);
		$query->bindParam(":image", $image, \PDO::PARAM_STR);

		if ($query->execute()){
	  		return TRUE;
	    }else{
	    	return false;
	    }

	}



	public static function suppDoc($id){ 

		$pdo = \utils\Connexion::getConnexion();

  	  	$query = $pdo->prepare("DELETE from documents where ref =:ref");
		$query->bindParam(":ref", $id, \PDO::PARAM_INT);

	    if ($query->execute()){
	      return  true;
	    } else {
	      return false;
	    }

	}

}

