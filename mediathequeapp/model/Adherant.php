<?php

namespace mediathequeapp\model;

/**
 *  La classe User
 *
 *  La Classe User  realise un Active Record sur la table page user
 */
class Adherant {
  /**
   *  Les attribut d'un utilisateur 
   *  @access private
   */
 
  private $ref,$nom,$prenom, $type, $descriptif, $auteur, $mdp, $image, $id, $login, $pass;


  //public $id, $login, $pass, $role; OSMAN
  
  /**
   *  Constructeur de User
   *
   *  fabrique une nouvelle categorie vide
   */
  public function __construct() {
    // rien à faire
  }


  /**
   *  Magic pour imprimer
   *
   *  Fonction Magic retournant une chaine de caracteres imprimable
   *  pour imprimer facilement un utilisateur pour la correction derreurs 
   *
   *  @return String
   */
  public function __toString() {
    return "[". __CLASS__ . "] [id : ". $this->id .
      "] [nom : " . $this->nom .
      "] [prenom : " . $this->prenom . "]";
  }

  /**
   *   Getter generique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
    throw new \Exception($emess);
  }
   
  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   */
  
  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) 
      $this->$attr_name=$attr_val; 
    else{
      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
      throw new \Exception($emess);
    }
  }

  /**
   *   Créer une session
   *
   *   Recherche dans la table adherent si l'id placé en parmaetre
   *   corrrespond à un adherent.
   *   Si l'adhérent existe on verifie la correspondance du mot de passe
   *   et on crée une session avec l'id
   *   @author willy
   *   @static
   *   @return renvoie un tableu d'objet de type Document
   */
  public static function connection($id,$mdp) {
    $pdo = \utils\Connexion::getConnexion();
    $query =$pdo->prepare("select id,mdp from adherants where id=:id");
    $query->bindParam(':id',$id);

    if($query->execute())
    {
      if($p = $query->fetch(\PDO::FETCH_OBJ)){
        if (password_verify($mdp, $p->mdp)) {
           $_SESSION['id'] = $p->id;
           return 'ok';
        }
        else{
          $res = 'Mot de passe invalide';
          return $res;
        }
      }
      else{
        $res = 'Aucun identifiant trouvé';
        return $res;
      }
      
    }
    else{
      $res = 'Erreur lors de la requête';
      return $res;
    }
  }

    /**
   *   Detruit la session
   *   @author willy
   *   @static
   */
  public static function deconnection() {
    if(isset($_SESSION['id']))
    {
      session_destroy();
    }
  }

  /**
   *   Finder sur ID
   *
   *   Retrouve la ligne de la table correspondant au ID passé en 
   *   paramètre, retourne un objet
   *  
   *   @static
   *   @param integer $id OID to find
   *   @return User renvoie un objet de type User
   */

  public static function findById($id) {
    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("select * from adherants where id=:id");
    $query->bindParam(':id',$id);



    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
    
    $o = new Adherant();

    $o->id     = $p->id;
    $o->nom    = $p->nom;
    $o->prenom = $p->prenom;
    $o->mail   = $p->mail;

    return $o;
  }
  
  

  /**
   *   Finder All
   *
   *   Renvoie toutes les lignes de la table User
   *   sous la forme d'un tableau d'objets
   *  
   *   @static
   *   @return Array renvoie un tableau de User ou vide
   */
  
  public static function findAll() {
    
    $res = array();
    $pdo = \utils\Connexion::getConnexion();

    $pdo_stm = $pdo->query("select * from adherants"); 
    if($pdo_stm){
      $rows = $pdo_stm->fetchAll(\PDO::FETCH_OBJ) ;
      foreach ($rows as $p){ 
      	$o = new Adherant();
      	$o->id     = $p->id;
      	$o->nom  = $p->nom;
      	$o->prenom   = $p->prenom;
      	$res[] = $o;
      }
    }
    return $res;
  }


  public static function recupereradherents($id) {
    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("SELECT * from adherants where id=:id");
    $query->bindParam(':id',$id);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
    
    $o = new Adherant();
    $o->id     = $p->id;
    $o->nom  = $p->nom;
    $o->prenom   = $p->prenom;

    return $o;
  }

   /**
   *   Liste les documents emprunté par un adhérent
   *   @author willy
   *   @static
   *   @return renvoie un tableu d'objet de type Document
   */
  public static function mesEmprunt($ref) {
    $pdo = \utils\Connexion::getConnexion();
    $query =$pdo->prepare("SELECT d.nom,d.genre,d.image,d.type,s.date_sortie,s.date_retour 
                            from sortie s
                            inner join documents d
                            on s.ref_doc = d.ref
                            where s.id_adherant =:ref");
    $query->bindParam(':ref',$ref);

    if($query->execute())
    {
      if($rows = $query->fetchAll(\PDO::FETCH_OBJ)){
        foreach ($rows as $p){
          $o = new Document();
          $o->nom   = $p->nom;
          $o->genre   = $p->genre;
          $o->image   = $p->image;
          $o->type   = $p->type;
          $o->date_sortie   = $p->date_sortie;
          $o->date_retour   = $p->date_retour;
          $res[] = $o;
        }
        return $res;
      }
      
    }
    else{
      echo'erreur requete';
    }
  }
  /**
  *
  *
  * retourn un tableau d'objet de tout les adherant
  *
  *
  **/
  public static function findAllAdherent(){ //récupérer les infos des adhérents

      $pdo = \utils\Connexion::getConnexion();
      
      $query = $pdo->query("SELECT * from adherants"); 

      if($rows = $query->fetchAll(\PDO::FETCH_OBJ)){
        foreach ($rows as $p){ 
          $o         = new Adherant();
          $o->id    = $p->id;
          $o->nom    = $p->nom;
          $o->prenom = $p->prenom;
          $res[]          = $o;
        }
        return $res;
      }

  }

    /**
  *
  * 
  * La fonction saveAdherent effectue une insertion dans la BDD.
  * On créé donc un nouvel adherent.
  *       @author : Osman
  **/

  function saveAdherent($nom, $prenom){
    $pdo = \utils\Connexion::getConnexion();
    
    //$query = $pdo->prepare("INSERT into adherants (`nom` , `prenom`, `mail` , `mdp` ) values (:nom, :prenom, :mail, :mdp)");
    $query = $pdo->prepare("INSERT into adherants (`nom` , `prenom`, `mdp` ) values (:nom, :prenom, :mdp)"); 

    $query->bindParam(':nom', $nom, \PDO::PARAM_STR);
    $query->bindParam(':prenom', $prenom,  \PDO::PARAM_STR);
    $query->bindParam(':mdp', $prenom,  \PDO::PARAM_STR);
    if ((!empty($nom)) && (!empty($prenom)) && ($query->execute())){

      $idadherent = $pdo->lastInsertId(); 
      return  $idadherent;
    } else {
      return false;
    }
  }     


  /**
  *
  * La fonction saveAdherent effectue une suppresion dans la BDD.
  * On supprime donc un nouvel adherent.
  *       @author : Osman
  **/
  public static function suppAdherent($id){ 

    $pdo = \utils\Connexion::getConnexion();

    $query = $pdo->prepare("DELETE from adherants where id =:id");
    $query->bindParam(':id', $id,  \PDO::PARAM_INT);

    if ($query->execute()){
      return  true;
    } else {
      return false;
    }
  }

  /**
  *
  * 
  * La fonction modifAdherent effectue une modification dans la BDD.
  * On met à jour les infos d'un adherent.
  *
  * @author : Osman
  **/
  public static function modifAdherent($id, $nom, $prenom){ 

    $pdo = \utils\Connexion::getConnexion();

    $query = $pdo->prepare("UPDATE `adherants` set `nom`=:nom,`prenom`=:prenom where `id`=:id");
    $query->bindParam(':id', $id,  \PDO::PARAM_INT);
    $query->bindParam(':nom', $nom,  \PDO::PARAM_STR);
    $query->bindParam(':prenom', $prenom,  \PDO::PARAM_STR);

    if ($query->execute()){
      return  true;
    } else {
      return false;
    }

  }
}
