<?php

namespace mediathequeapp\model;

/**
 *  La classe User
 *
 *  La Classe User  realise un Active Record sur la table page user
 */
class Bibliothecaire {

  /**
   *  Les attribut d'un utilisateur 
   *  @access private
   */
  private $id, $nom, $prenom,$ref,$type,$name,$documents,$ref_doc,$id_adherant,$date_retour;
  
  /**
   *  Constructeur de User
   *
   *  fabrique une nouvelle categorie vide
   */
  public function __construct() {
    // rien à faireee
  }


  /**
   *  Magic pour imprimer
   *
   *  Fonction Magic retournant une chaine de caracteres imprimable
   *  pour imprimer facilement un utilisateur pour la correction derreurs 
   *
   *  @return String
   */
  public function __toString() {
    return   " ".$this->nom." ".$this->prenom;
  }

  /**
   *   Getter generique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
    throw new \Exception($emess);
  }
   
  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   */
  
  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) 
      $this->$attr_name=$attr_val; 
    else{
      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
      throw new \Exception($emess);
    }
  }


  /**
   *   Sauvegarde dans la base
   *
   *   Enregistre l'etat de l'objet dans la table
   *   Si l'objet possede un identifiant : 
   *         mise à jour de la ligne correspondante (update)
   *   sinon  
   * 	     insertion dans une nouvelle ligne insert
   *
   *   @return int le nombre de lignes touchees
   */
  
  public function save() {
    if (isset($this->id)) 
      return $this->update();
    else {
      // verification si un untilisateur existe avec le même login existe 
      $r=self::findByName($this->login);
      if ($r==false) 
	return $this->insert();
      else {
	//lutilisateur exite deja met à jour id et on update
	$this->id=$r->id;
	return $this->update();
      }
    }
  }

  /**
   *   mise a jour de la ligne courante
   *   
   *   Sauvegarde l'objet courant dans la base en faisant un update
   *   méthode privée - la méthode publique s'appelle save
   *   @acess public
   *   @return boolean
   */
  private function update() {
    $pdo = \utils\Connexion::getConnexion();
	
    //preparation de la requete
    try{
      $query = $pdo->prepare("update user set login=:login, pass=:pass, role=:role where id=:id");
      
      //liaison des parametres
      $query->bindParam(':login', $this->login, \PDO::PARAM_STR);
      $query->bindParam(':pass',  $this->pass,  \PDO::PARAM_STR);
      $query->bindParam(':role',  $this->role,  \PDO::PARAM_INT);
      $query->bindParam(':id',    $this->id);
      
      //lancement de la requete préparée	  
      return $query->execute();
    }
    catch(\PDOException $e){ 
      $emess = __CLASS__ . ": Erreur l'ors de update (".$e->getMessage().")";
      throw new \Exception($emess);	
    }
  }


  /**
   *   Suppression dans la base
   *
   *   Supprime la ligne dans la table corrsepondant à l'objet courant
   *   L'objet doit posséder un ID
   */
  public function delete() {
    
    /* Vérifier ce que nous effaçons est bien dans la base */
    if ( !isset($this->id) ) 
      return false;

    $pdo = \utils\Connexion::getConnexion();
  
    $query = $pdo->prepare("delete from user where id = :id");
    $query->bindParam(":id", $this->id);
    return $query->execute(); 
  }
		
  /**
   *   Insertion dans la base
   *
   *   Insère l'objet comme une nouvelle ligne dans la table page
   *   l'objet ne doit pas posséder un id
   *
   *   @return l'id de la page de la ligne insérée ou -1 en ca d'erreur 
   */	
								
  public function insert() {
    $pdo = \utils\Connexion::getConnexion();
    
    $query = $pdo->prepare(
	      "insert into user (login, pass, role) values (?,?,?)"); 

    $query->bindParam(1, $this->login, \PDO::PARAM_STR);
    $query->bindParam(2, $this->pass,  \PDO::PARAM_STR);
    $query->bindParam(3, $this->role,  \PDO::PARAM_STR);
    if( $query->execute() ){
      $this->id = $pdo->LastInsertId();
      return $this->id;
    }
    else
      return -1;
  }
  
  /**
   *   Finder sur ID
   *
   *   Retrouve la ligne de la table correspondant au ID passé en 
   *   paramètre, retourne un objet
   *  
   *   @static
   *   @param integer $id OID to find
   *   @return User renvoie un objet de type User
   */


  public static function findById($id) {
    $pdo = \utils\Connexion::getConnexion();
    
    $query =$pdo->prepare("select * from adherants where id=:id");
    $query->bindParam(':id',$id);

    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
    
    $o = new Adherant();
    $o->id     = $p->id;
    $o->nom  = $p->nom;
    $o->prenom   = $p->prenom;
    

    return $o;
  }
  /**
   *   Finder sur title
   *
   *   Retrouve la ligne de la table correspondant au titre passé en paramètre,
   *   retourne un objet ou false si la page n'existe pas
   *  
   *   @static
   *   @param $title le tire de la page cherchée
   *   @return User renvoie un objet de type User
   */

  public static function findByName($name) {
    
    $pdo = \utils\Connexion::getConnexion();
    
    $query = $pdo->prepare("select * from user where login=:login");
    $query->bindParam(":login", $name);
    
    if( !$query->execute() )
      return false;
    
    $p = $query->fetch(\PDO::FETCH_OBJ) ;
    if (! $p)
      return false;
        
    $o = new User();
    $o->id     = $p->id;
    $o->login  = $p->login;
    $o->pass   = $p->pass;
    $o->role   = $p->role;
    return $o;
  }



  /**
   *   Finder All
   *
   *   Renvoie toutes les lignes de la table User
   *   sous la forme d'un tableau d'objets
   *  
   *   @static
   *   @return Array renvoie un tableau de User ou vide
   */
  
  public static function findAll() {
    
    $res = array();
    $pdo = \utils\Connexion::getConnexion();
    
    $pdo_stm = $pdo->query("select * from user"); 
    if($pdo_stm){
      $rows = $pdo_stm->fetchAll(\PDO::FETCH_OBJ) ;
      foreach ($rows as $p){ 
	$o = new User();
	$o->id     = $p->id;
	$o->login  = $p->login;
	$o->pass   = $p->pass;
	$o->role   = $p->role;
	$res[] = $o;
      }
    }
    
    return $res;
  }
  
  /*
   * Méthodes de maintien de l'association
   *
   */

  /*
   * Methos pour récupérer les page d'un auteur.
   *
   * @return array  un tableau d'objet Page (les page de l'auteur)  
   */
  
  public function pages(){
    $res = array();
    $pdo = \utils\Connexion::getConnexion();

    /* Compléter ici 
     * - Récupérer toute les pages de l'auteur depuis la table page 
     * - Pour chacune créer un objet Page et le valuer ses attribut 
     * - Retourner le tableau ou false si erreur
     *
     */

    //BEGIN_HIDE
    
    $query = $pdo->prepare("select * from page where author=:author");
    $query->bindParam(":author", $this->id);

    if( !$query->execute() )
      return false;
       
    $rows = $query->fetchAll(\PDO::FETCH_OBJ) ;
    foreach ($rows as $p){ 
      $o = new Page();
      $o->id     = $p->id;
      $o->title  = $p->title;
      $o->text   = $p->text;
      $o->date   = $p->date;
      $o->author = $p->author;
      $res[] = $o;
    }

    //END_HIDE
    
    return $res;
  }

}