<?php

/**
*  @author: Mimuid
*/
namespace mediathequeapp\control;

class MediathequeControler{
	

	function dispatch($http){
		switch ($http->action) {
			case 'emprunt':
				$this->emprunt($http);
				break;
			case 'restituer':
				$this->restituer($http);
				break;
			case 'etat':
				$this->etatAction($http);
				break;
			case 'formetat':
				$this->formetatAction($http);
				break;

			case 'ajout':
				$this->ajoutAction($http);
				break;
			case 'formajout':
				$this->formajoutAction($http);
				break;
			case 'supp':
				$this->suppAction($http);
				break;
			case 'formsupp':
				$this->formsuppAction($http);
				break;
			case 'modif':
				$this->modifAction($http);
				break;
			case 'formmodif':
				$this->formmodifAction($http);
				break;
			case 'ajoutdoc':
				$this->ajoutDocAction($http);
				break;
			case 'formajoutdoc':
				$this->formajoutDocAction($http);
				break;
			case 'suppdoc':
				$this->suppDocAction($http);
				break;
			case 'formsuppdoc':
				$this->formsuppDocAction($http);
				break;
			case 'afficheadherent':
				$this->AllAdherentAction($http);
				break;
			case 'formafficheadherent':
				$this->formAllAdherentAction($http);
				break;

			case 'reserve':
				//fonction de willou
				$this->reserveAction($http);
				break;
		}
	}
	

	/**
	*
	*	Permet tous les documents 
	*
	**/
	function listeDocument($http){
		$p = \mediathequeapp\model\Document::findDocument(null,null,null); 
		if ($p){
			$v = new \mediathequeapp\view\ViewAdherant();
			$v->liste = $p;
			$v->racine = $http->racine;
			$v->afficher( 'liste' );
		}	
	}

	/**
	*
	*	Permet de rechercher un document en particulier grâce à sa référence
	*
	**/
	function pageDocument($http){
		if ($http->param) {
			$ref = $http->param;
		}
		$p = \mediathequeapp\model\Document::findDocumentByRef($ref); 
		if ($p){
			$v = new \mediathequeapp\view\ViewAdherant();
			$v->racine = $http->racine;
			$v->page = $p; 
			$v->afficher('page');
		}	
	}


	/**
	*
	*	
	*	La fonction etatAction permet d'insérer l'état d'un média dans la table "sortie"
	*	@author : Osman
	*
	**/
	function etatAction($http){

		$refdoc = $_POST['idmedia'];
		$etat = $_POST['etat'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;

		$verif = \mediathequeapp\model\Document::verifier($refdoc);
		if ($verif){
			$p = \mediathequeapp\model\Document::saveEtat($refdoc, $etat);		
		}
		else	{
			$p = \mediathequeapp\model\Document::updateEtat($refdoc, $etat);
		}
		if ($p){
			$v = new \mediathequeapp\view\ViewBibliothecaire();
			$v->racine = $http->racine;
			$v->afficher('retouretat');
		}
	}

	/**
	*
	*	
	*	La fonction formetatAction affiche le formulaire de saisie pour
	*	enregistrer l'état du média dans la table "sortie"
	*       @author : Osman
	**/
	function formetatAction($http) {

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;

		$v->afficher('formetat');
	}

	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/


	/**
	*
	*	
	*	La fonction ajoutAction permet d'ajouter un adhérent dans la BDD
	*	@author : Osman
	*
	**/

	function ajoutAction($http){

		$nom = $_POST['nomadherent'];
		$prenom = $_POST['prenomadherent'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Adherant::saveAdherent($nom, $prenom); 
		if ($p != false){
			$v->id = $p;
			$v->afficher('retourformajoutsucces');
		}	else{
			$v->afficher('retourformajoutechec');
		}
		
	}

	/**
	*
	*	
	*	La fonction formajoutAction affiche le formulaire pour ajouter un adhérent
	*	@author : Osman
	*
	**/

	function formajoutAction($http){
		
		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;

		$v->afficher('formajout');
		
	}

	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/


	/**
	*
	*	
	*	La fonction suppAction permet de supprimer un adhérent
	*	
	*       @author : Osman
	**/

	function suppAction($http){
		
		$id = $_POST['idadherent'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Adherant::suppAdherent($id); 
		if ($p != false){
			$v->id = $p;
			$v->afficher('retourformsuppsucces');
		}	else{
			$v->afficher('retourformsuppechec');
		}
		
	}

	/**
	*
	*	
	*	La fonction emprunt affiche le formulaire de saisie pour
	*	enprunter un document @author Anthony
	*
	**/
	function emprunt($http){
		if (!empty($_POST["id"])) {
			$id=$_POST["id"];
			$adherents = \mediathequeapp\model\Adherant::recupereradherents($id);
			$vue = new \mediathequeapp\view\ViewBibliothecaire();
			$tabRef  = array();
			for ($i=0; $i < 6 ; $i++) {
				$ref = $_POST["reference".$i];
				if (!empty($ref)) {
					$tabRef[] = $ref;
				}
			}
			$doc = \mediathequeapp\model\Document::recupererdocuments($tabRef);
			foreach ($doc as $key => $value) {
				$verifdejaemprunter= \mediathequeapp\model\Document::verifier($value->ref);
				$verifierreservation=\mediathequeapp\model\Document::verifierreservation($value->ref,$id);
      				if ($verifdejaemprunter) {

      					if ($verifierreservation != false) /*verif si reserve */{
      						if ($verifierreservation == $adherents->id) {
      							$sortie= \mediathequeapp\model\Document::sortiedocuments($value->ref,$adherents->id);
      							$supprimerreservation= \mediathequeapp\model\Document::supprimereservation($value->ref);


      						}//sinon rien ou msg erreur
      					}	else{
      						$sortie= \mediathequeapp\model\Document::sortiedocuments($value->ref,$adherents->id);
      					}
	  				}	else{
						$djemprunte = \mediathequeapp\model\Document::text();
						$vue->noemprunt = $djemprunte;
					}
	  			$date = \mediathequeapp\model\Document::recupereradherents($adherents->id);
			}

			$vue->racine = $http->racine;
			$vue->sortie = $adherents;
			if (!empty($doc)) {
			$vue->date_retour = $date;
				
			}
			$vue->liste = $doc;
			$vue->afficher('emprunter');
      	}	else{
		$vue = new \mediathequeapp\view\ViewBibliothecaire();
				$vue->racine = $http->racine; 
			$vue->afficher('emprunter');
		}

	}
	

	/**
	*
	*	
	*	La fonction formsuppAction permet d'afficher le formulaire pour supprimer un adhérent
	*	
	*       @author : Osman
	**/


	function formsuppAction($http){

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;

		$v->afficher('formsupp');
	}

	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/


	function modifAction($http){
		
		$nom = $_POST['nom'];
		$prenom = $_POST['prenom'];
		$id = $_POST['id'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Adherant::modifAdherent($id, $nom, $prenom);
		$tab = \mediathequeapp\model\Adherant::findAllAdherent();
		$v->tab = $tab;
		if ($p != false){
			$v->id = $p;
			$v->afficher('retourformmodifsucces');
		}	else{
			$v->afficher('retourformmodifechec');
		} 
	}


	function formmodifAction($http){
		$id = $_GET['id'];
		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Adherant::recupereradherents($id);
		if ($p){
			$v->id = $p;
			$v->afficher('formmodif');
		}
	}

	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/


	function AllAdherentAction($http){
		
		$id = $_POST['idadherent'];
		$mail = $_POST['mailadherent'];


		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Document::findAllAdherent();
		$tab = $p;

	}


	function formAllAdherentAction($http){

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$tab = \mediathequeapp\model\Adherant::findAllAdherent();
		$v->tab = $tab;
		$v->afficher('formafficheadherent');

	}


	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/


	/**
	*
	*	
	*	La fonction restituer affiche le formulaire de saisie pour
	*	restituer un document @author Anthony
	*
	**/
function restituer($http){
		$vue = new \mediathequeapp\view\ViewBibliothecaire();
			$tabRef  = array();
			for ($i=0; $i < 6 ; $i++) {
				if (!empty($_POST["reference".$i])) {
				$ref = $_POST["reference".$i];
				if (!empty($ref)) 
				{
					$tabRef[] = $ref;
					$doc = \mediathequeapp\model\Document::recupererdocuments($tabRef);
					if (!empty($doc)) {
					foreach ($doc as $key => $value) 
					{
						$r= \mediathequeapp\model\Document::verifier($value->ref);
					}
						$documents = \mediathequeapp\model\Document::recupererdocumentsadherents($value->ref);
						if (!empty($documents)) {
						$adherents = \mediathequeapp\model\Adherant::recupereradherents($documents->id_adherant);
						echo $value->id_adherant;
						$rende = \mediathequeapp\model\Document::rendredocument($value->ref);
						$date = \mediathequeapp\model\Document::recupereradherents($value->ref);
						$mesdocs = \mediathequeapp\model\Adherant::mesEmprunt($adherents->id);
						$vue->mesdocuments = $mesdocs;
						$vue->sortie = $adherents;
						$vue->liste = $doc;
						$vue->dates = $date;
						}
						else{
						$vue->norefe = "false";

						
						}
					}
				}
			

			}
					
				}
			$vue->racine = $http->racine;  
			$vue->afficher('restituer');

	}
	
	/**	
	*
	*	La fonction ajoutDocAction permet d'ajouter un document dans la BDD
	*	
	*       @author : Osman
	**/

	function ajoutDocAction($http){

		$type       = $_POST['typedoc'];
		$genre      = $_POST['genredoc'];
		$nom        = $_POST['nomdoc'];
		$descriptif = $_POST['descriptifdoc'];
		$auteur     = $_POST['auteurdoc'];
		$newtype    = $_POST['newtypedoc'];
		$newgenre   = $_POST['newgenredoc'];
		if (!empty($_FILES['file'])) {
	    	$image = str_replace(" " , '_' , $nom);
	    	$image = str_replace("'" , '_' , $image);
	    	$image = str_replace("\"" , '_' , $image);
			if (0 === $_FILES['file']['error']) {
				if ($newtype!=null) {
					mkdir(getcwd().'/images/'.$newtype);
					chmod(getcwd().'/images/'.$newtype, 0755);
					$uploadDirectory = getcwd().'/images/'.$newtype.'/';
				}	else{
					$uploadDirectory = getcwd().'/images/'.$type.'/';
				}
	        	$fileInfo = new \SplFileInfo($_FILES['file']['name']);
	        	$extension = $fileInfo->getExtension();
 
	        	$extensionPossible = array("png", "jpg", "bmp", "gif", "jpeg");

	        	if (in_array($extension, $extensionPossible)){
		        	$newFile = $image.'.'.$extension;
		        	$image = $newFile;
		        	if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadDirectory.$newFile)) {
						if ($newtype!=null) {
							chmod(getcwd().'/images/'.$newtype.'/'.$image, 0755);
						}	else{
							chmod(getcwd().'/images/'.$type.'/'.$image, 0755);
						}
		        		$v = new \mediathequeapp\view\ViewBibliothecaire();
						$v->racine = $http->racine;
						
						if (($newgenre!=null)&&($newtype!=null)){
							$p = \mediathequeapp\model\Document::ajoutDoc($newtype, $newgenre, $nom, $descriptif, $auteur, $image);  
						}
						if (($newgenre==null)&&($newtype!=null)){
							$p = \mediathequeapp\model\Document::ajoutDoc($newtype, $genre, $nom, $descriptif, $auteur, $image);  
						}
						if (($newgenre!=null)&&($newtype==null)){
							$p = \mediathequeapp\model\Document::ajoutDoc($type, $newgenre, $nom, $descriptif, $auteur, $image);  
						}
						if (($newgenre==null)&&($newtype==null)){
							$p = \mediathequeapp\model\Document::ajoutDoc($type, $genre, $nom, $descriptif, $auteur, $image);  
						}
		        	}	else{
		        		echo "Fail : move_uploaded_file()";
		        	}
		        }	else {
					echo "Fail : extension";
		        }
	        }
		}
		$genre = \mediathequeapp\model\Document::findGenre();
		$type = \mediathequeapp\model\Document::findType();
		$v->doc = $genre;
		$v->tab = $type;
		if ($p){
			$v->afficher('retourformadddocsucces');
		}	else{
			$v->afficher('retourformadddocechec');
		} 

	}

	/**
	*
	*	
	*	La fonction formajoutDocAction affiche le formulaire d'ajout d'un document
	*	
	*       @author : Osman
	**/

	function formajoutDocAction($http){
		
		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$genre = \mediathequeapp\model\Document::findGenre();
		$type = \mediathequeapp\model\Document::findType();
		$v->doc = $genre;
		$v->tab = $type;
		$v->afficher('formajoutdoc');
		
	}

	/**********************************************************************/
	/**********************************************************************/	
	/**********************************************************************/



	/**
	*
	*	
	*	La fonction suppDocAction permet de supprimer un document de la BDD
	*	
	*       @author : Osman
	**/

	function suppDocAction($http){
		
		$id = $_POST['iddoc'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Document::suppDoc($id);
		if ($p != false){
			$v->id = $p;
			$v->afficher('retourformsuppdocsucces');
		}	else{
			$v->afficher('retourformsuppdocechec');
		} 

	}

	/**
	*
	*	
	*	La fonction formsuppDocAction affiche le formulaire de suppression d'un document
	*	
	*       @author : Osman
	**/


	function formsuppDocAction($http){

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;

		$v->afficher('formsuppdoc');

	}


	function reserveAction($http){

		$refDoc = $_POST['idmedia'];
		$id_adherant = $_POST['id_adherant'];

		$v = new \mediathequeapp\view\ViewBibliothecaire();
		$v->racine = $http->racine;
		
		$p = \mediathequeapp\model\Document::reserverDocument($refDoc,$id_adherant);
		
		$v->afficher('retouretat');
	}

}
