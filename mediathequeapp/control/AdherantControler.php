<?php

/**
*  @author: Mimuid
*/
namespace mediathequeapp\control;

class AdherantControler{
	

	function dispatch($http){
		switch ($http->action) {
			case 'recherche':
				$this->rechercheAction($http);
				break;
			case 'filtrage':
				$this->filtreAction($http);
				break;
			case 'mesEmprunt':
				if(isset($_SESSION['id'])){
			      $this->mesEmprunt($http);
			    }   else{
			    	$this->connection($http);
			    }
				break;
			case 'connection':
				$this->connection($http);
				break;
			case 'deconnection':
				$this->deconnection($http);
				break;
			case 'connectionVerif':
				$this->connectionVerif($http);
				break;

			case 'listeDocument':
				$this->listeDocument($http);
				break;
			case 'pageDocument':
				$this->pageDocument($http);
				break;
			case 'reserver':
				$this->reserver($http);
				break;
		}
	}
	
	/**
	*
	*	Requete pour effectué les requete de recherche dans la bdd
	*
	**/
	function filtreAction($http){
		$keyword = $_POST["keyword"];
		$genre = $_POST["genre"];
		$categorie = $_POST["categorie"];

		if ((!empty($categorie))&&($categorie!="all")) {
			if ((!empty($genre))&&($genre!="all")) {
				if (!empty($keyword)) {
					$p = \mediathequeapp\model\Document::findDocument($categorie, $genre, $keyword); 
				}	else{
					$p = \mediathequeapp\model\Document::findDocument($categorie, $genre,null); 
				}
			}	elseif(!empty($keyword)){
				$p = \mediathequeapp\model\Document::findDocument($categorie,null, $keyword); 
			}	else{
				$p = \mediathequeapp\model\Document::findDocument($categorie,null,null); 
			}
		}	else{
			if ((!empty($genre))&&($genre!="all")) {
				if (!empty($keyword)) {
					$p = \mediathequeapp\model\Document::findDocument(null,$genre, $keyword); 
				}	else{
					$p = \mediathequeapp\model\Document::findDocument(null,$genre,null);
				}
			}	elseif(!empty($keyword)){
				$p = \mediathequeapp\model\Document::findDocument(null,null,$keyword); 
			}
		}
		$v = new \mediathequeapp\view\ViewAdherant();
		$v->liste = $p;
		$v->racine = $http->racine;
		$v->afficher( 'liste' );
	}
	
	/**
	*
	*	Requete pour crée le formulaire de recherche
	*	La liste des genres est generée de manière dynamique grâce à la lecture dans la bdd,
	*	ils sont stoker dans un tableau (genre) de l'objet ViewAdherant 
	*
	**/
	function rechercheAction($http){
		$v = new \mediathequeapp\view\ViewAdherant();
		$v->racine = $http->racine;
		$p = \mediathequeapp\model\Document::findGenre(); 
		$v->genre = $p;
		$v->afficher('formSearch');
	}

	/**
	*
	*	Permet d'afficher les emprunts en cour d'un adherent
	*
	**/
	function mesEmprunt($http){
		$ref = $_SESSION['id'];
		$p = \mediathequeapp\model\Adherant::mesEmprunt($ref);
		$v = new \mediathequeapp\view\ViewAdherant();
		$v->liste = $p;
		$v->racine = $http->racine;
		$v->afficher('emprunt');
	}

		/**
	*
	*	Affiche la page de connection
	*
	**/
	function connection($http){
		$v = new \mediathequeapp\view\ViewAdherant();
		if(!isset($http->param)){
			$message = $http->param;
			$v->message = $message;
		}
		$v->racine = $http->racine;
		$v->afficher('connection');
	}

	/**
	*
	*	Permet de détruire la session en cour
	*
	**/
	function deconnection($http){
		$v = new \mediathequeapp\view\ViewAdherant();
		$p = \mediathequeapp\model\Adherant::deconnection();
		$v->racine = $http->racine;
		header('Location: connection');
	}

	/**
	*
	*	Vérifie l'id et le mot de passe de l'adherant et crée la session
	*
	**/
	function connectionVerif($http){
		$id = $_POST['id'];
		$mdp = $_POST['mdp'];
		$p = new \mediathequeapp\model\Adherant();
		$res = $p->connection($id,$mdp);
		$v = new \mediathequeapp\view\ViewAdherant();
		$v->racine = $http->racine;
		if(isset($res)){
			if($res == 'ok'){
				header('Location: mesEmprunt');
			}
			else{
				$v->message = $res;
				header("Location: connection/".$res."");
			}
		}
		
	}

	/**
	*
	*	Permet tous les documents 
	*
	**/
	function listeDocument($http){
		$v = new \mediathequeapp\view\ViewAdherant();
		if(!isset($http->param)){
			$message = $http->param;
			$v->message = $message;
		}
		$p = \mediathequeapp\model\Document::findDocument(null,null,null); 
		if ($p){
			$v->liste = $p;
			$v->racine = $http->racine;
			$v->afficher( 'liste' );
		}	
	}

	/**
	*
	*	Permet de rechercher un document en particulier grâce à sa référence
	*
	**/
	function pageDocument($http){
		if ($http->param) {
			$ref = $http->param;
		}
		$p = \mediathequeapp\model\Document::findDocumentByRef($ref); 
		if ($p){
			$v = new \mediathequeapp\view\ViewAdherant();
			$v->racine = $http->racine;
			$v->page = $p; 
			$v->afficher('page');
		}	
	}

	/**
	*
	*	Permet de reserver un document grâce à sa référence
	*
	**/
	function reserver($http){
		$refDoc = $http->param;
		$id_adherant = $_SESSION['id'];
		$p = new \mediathequeapp\model\Document();
		$res = $p->reserverDocument($refDoc,$id_adherant);
		$v = new \mediathequeapp\view\ViewAdherant();
		$v->racine = $http->racine;
		header("Location: /$http->racine/index.php/AdherantControler/listeDocument/".$res."");
	}
}