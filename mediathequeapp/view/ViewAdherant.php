<?php
namespace mediathequeapp\view;
/**
* @author : Mimuid
*/
class ViewAdherant{
	
	private $page, $racine, $liste, $genre, $message;
	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) 
			$this->$attr_name=$attr_val; 
		else{
			$emess = __CLASS__ . ": unknown member $attr_name (__set)";
			throw new \Exception($emess);
		}
	}

	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
			return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new \Exception($emess);
	}

	function afficher($param){
		$header = $this->afficheHead();
		$nav    = $this->afficheNav($param);
		switch ($param) {
				case 'page':
					$main = $this->afficheDocument();
					break;
				case 'liste':
					$main = $this->afficheListeDocument();
					break; 
				case 'edit':
					$main = $this->afficheEdition();
					break;
				case 'save':
					$main = $this->afficheSave();
					break;
				case 'formSearch':
					$main = $this->afficheForm();
					break;
				case 'emprunt':
					$main = $this->afficheEmprunt();
					break;
				case 'connection':
					$main = $this->afficheConnection();
					break;
			}
		$html ="<!DOCTYPE html>
				<html>
				<head> 
					<title>TdWiki</title>
					<meta http-equiv='content-type' content='text/html; charset=UTF-8'>
					<link rel='stylesheet' type='text/css' href='/$this->racine/css/css/style.css'>
					<meta name='viewport' content='initial-scale=1, maximum-scale=1'>

				</head>
				<body>";
		$html.= "<header>".$header."</header>";
		$html.= "<section id='content'><nav>".$nav."</nav>";
		$html.= "<section>".$main."</section></section>";
		$html.= "</body></html>";
		echo $html;
	}
	function afficheDocument(){
		$tab = $this->page;
		$l = "<ul id='afficheDoc'>";
			$l.="<li><img src='/$this->racine/images/".$tab->type."/".$tab->image.".jpg' alt='".$tab->nom."' /></li>";
			$l.="<li><h1>".$tab->nom."</h1></li>";
			$l.="<li><h4>".$tab->auteur."</h4></li>";
			$l.="<li><h6>".$tab->type."</h6></li>";
			if($tab->etat != ''){
				$l.="<li><h6>Etat: ".$tab->etat." - Date de retour: ".$tab->date_retour."</h6></li>";
			}
			else{
				$l.="<li><h6>Etat: Disponible</h6></li>";
			}
			
			$l.="<li><p>".$tab->descriptif."</p></li>";
			if(isset($_SESSION['id']))
		    {
		      $l.="<li><a class='bouton bouton-small bouton-bleu' href='/$this->racine/index.php/AdherantControler/reserver/".$tab->ref."'>Réserver</a></li>";
		    }
			
		$l.= "</ul>";
		return $l;
	}

	function afficheHead(){
		return "<h1>Mediatheque</h1>";
	}

	function afficheNav($param){

		$n="<ul class='navbar'>";
		switch ($param) {
			case 'liste':
				$n.="<li class='active'><a href='/$this->racine/index.php/AdherantControler/listeDocument'>Liste des documents</a></li>
				<li><a href='/$this->racine/index.php/AdherantControler/recherche'>Recherche</a></li>";

				if(isset($_SESSION['id']))
			    {
			      $n.="<li><a href='/$this->racine/index.php/AdherantControler/mesEmprunt'>Mes emprunts</a></li>
					<li><a href='/$this->racine/index.php/AdherantControler/deconnection'>Se deconnecter</a></li>";
			    }
			    if(!isset($_SESSION['id']))
			    {
			      $n.="<li><a href='/$this->racine/index.php/AdherantControler/connection'>Se connecter</a></li>";
			    }	
			break;
			case 'formSearch':
			case 'page':
				$n.="<li ><a href='/$this->racine/index.php/AdherantControler/listeDocument'>Liste des documents</a></li>
				<li class='active'><a href='/$this->racine/index.php/AdherantControler/recherche'>Recherche</a></li>";

				if(isset($_SESSION['id']))
			    {
			      $n.="<li><a href='/$this->racine/index.php/AdherantControler/mesEmprunt'>Mes emprunts</a></li>
					<li><a href='/$this->racine/index.php/AdherantControler/deconnection'>Se deconnecter</a></li>";
			    }
			    if(!isset($_SESSION['id']))
			    {
			      $n.="<li><a href='/$this->racine/index.php/AdherantControler/connection'>Se connecter</a></li>";
			    }	
			break;
			case 'connection':
				$n.="<li ><a href='/$this->racine/index.php/AdherantControler/listeDocument'>Liste des documents</a></li>
				<li><a href='/$this->racine/index.php/AdherantControler/recherche'>Recherche</a></li>";

				if(isset($_SESSION['id']))
			    {
			      $n.="<li><a href='/$this->racine/index.php/AdherantControler/mesEmprunt'>Mes emprunts</a></li>
					<li><a href='/$this->racine/index.php/AdherantControler/deconnection'>Se deconnecter</a></li>";
			    }
			    if(!isset($_SESSION['id']))
			    {
			      $n.="<li class='active'><a href='/$this->racine/index.php/AdherantControler/connection'>Se connecter</a></li>";
			    }	
			break;
			case 'emprunt':
				$n.="<li ><a href='/$this->racine/index.php/AdherantControler/listeDocument'>Liste des documents</a></li>
				<li><a href='/$this->racine/index.php/AdherantControler/recherche'>Recherche</a></li>";

				if(isset($_SESSION['id']))
			    {
			      $n.="<li class='active'><a href='/$this->racine/index.php/AdherantControler/mesEmprunt'>Mes emprunts</a></li>
					<li><a href='/$this->racine/index.php/AdherantControler/deconnection'>Se deconnecter</a></li>";
			    }
			    if(!isset($_SESSION['id']))
			    {
			      $n.="<li ><a href='/$this->racine/index.php/AdherantControler/connection'>Se connecter</a></li>";
			    }
			break;
		}
				
			    
		$n.="</ul>";
		return $n;
	}
	function afficheListeDocument(){
		$tab = $this->liste;
		$l = '';
		$message = $this->message;
		if(isset($message)){
			if($message == 'ok'){
				$l.= "<div class='alert-box-success'>Réservation réussi</div>";
			}
			else{
				$l.= "<div class='alert-box-danger'>".$message."</div>";
			}
			
		}
		foreach ($tab as $key => $value) {
			$l.="<img src='/$this->racine/images/".$value->type."/".$value->image.".jpg' alt='".$value->nom."' />";
			$l.= "<ul>";
				$l.="<li><h2>".$value->nom."</h2></li>";
				$l.="<li><h6>".$value->genre."</h6></li>";
				$l.="<li><h6>".$value->type."</h6></li>";
				$l.="<li><a class='bouton bouton-small bouton-bleu' href='/$this->racine/index.php/AdherantControler/pageDocument/$value->ref'>Plus d'info</a></li>";
			$l.= "</ul>";
		}
		return $l;
	}

	function afficheForm(){
		$f = "<form method='post' id='recherche' action='/$this->racine/index.php/AdherantControler/filtrage'>
		<select name='categorie'>
  			<option value='all'>Toutes catégories</option> 
  			<optgroup label='Catégorie'>
  			<option value='CD'>CD</option> 
  			<option value='DVD'>DVD</option>
  			<option value='livre'>Livre</option>
  			</optgroup>
		</select>
		<select name='genre'>
  			<option value='all'>Touts genres</option> 
  			<optgroup label='Catégorie'>";
  		foreach ($this->genre as $key => $value) {
			$f.="<option value='".$value."' class='firstLetter'>".$value."</option>";
		}
		$f.="</optgroup>
		</select>
		<input type='text'  id='keyword' name='keyword' placeholder='Mot clef'>
		<input type='submit' class='bouton bouton-small bouton-bleu'  value='Rechercher'></from>";
		return $f;
	}

	function afficheEmprunt(){
		$tab = $this->liste;
		$l = "<h1>Mes emprunts</h1>";
		
		foreach ($tab as $key => $value) {
			$l.="<img src='/$this->racine/images/".$value->type."/".$value->image."' alt='".$value->nom."' />";
			$l.= "<ul>";
				$l.="<li><h2>".$value->nom."</h2></li>";
				$l.="<li><h6>".$value->genre."</h6></li>";
				$l.="<li><h6>Date de sortie: ".$value->date_sortie."</h6></li>";
				$l.="<li><h6>Date de retour: ".$value->date_retour."</h6></li>";
			$l.= "</ul>";
		}
		return $l;
	}
	function afficheConnection(){
		$l= "<h1>Se connecter</h1>";
		$message = $this->message;
		if(isset($message)){
			$l.= "<div class='alert-box-danger'>".$message."</div>";
		}
		$l.="<form class='connection' method='POST' action='/$this->racine/index.php/AdherantControler/connectionVerif'>
			<label>Identifiant</label>
			<input type='text' name='id' placeholder='Identifiant'/>
			<label>Mot de passe</label>
			<input type='password' name='mdp' placeholder='Mot de passe'/>
            <button class='bouton bouton-small bouton-bleu'>Se connecter</button>
        </form>";

		return $l;
	}
}
