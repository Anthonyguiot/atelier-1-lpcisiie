<?php
namespace mediathequeapp\view;
/**
* @author : Mimuid
*/

class ViewBibliothecaire{
	


	private $page, $dates, $racine, $liste, $doc, $documents, $id, $sortie, $noemprunt, $documentsid, $date_retour, $tab, $mesdocuments,$norefe;

	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) 
			$this->$attr_name=$attr_val; 
		else{
			$emess = __CLASS__ . ": unknown member $attr_name (__set)";
			throw new \Exception($emess);
		}
	}

	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
			return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new \Exception($emess);
	}

	function afficher($param){
		$header = $this->afficheHead();
		$nav    = $this->afficheNav($param);
		switch ($param) {
				case 'list':
					$main = $this->afficheListe();
					break; 
				case 'emprunter':
					$main = $this->afficheEmprunt();
					break;
				case 'restituer':
					$main = $this->afficheRestituer();
					break;
				case 'formetat':
					$main = $this->afficheEtat();
					break;
				case 'retouretat':
					$main = $this->afficheEtat();
					$main.= $this->afficheEtatRetour();
					break;
				case 'formajout':
					$main = $this->ajoutAdherent();
					break;
				case 'retourformajoutsucces':
					$main = $this->ajoutAdherent();
					$main.= $this->afficheAjoutRetourSucces();
					break;
				case 'retourformajoutechec':
					$main = $this->ajoutAdherent();
					$main.= $this->afficheAjoutRetourEchec();
					break;
				case 'retourformsuppsucces':
					$main = $this->suppAdherent();
					$main.= $this->afficheSuppRetourSucces();
					break;
				case 'retourformsuppechec':
					$main = $this->suppAdherent();
					$main.= $this->afficheSuppRetourEchec();
					break;
				case 'retourformmodifsucces':
					$main = $this->afficheAllAdherent();
					$main.= $this->afficheModifRetourSucces();
					break;
				case 'retourformmodifechec':
					$main = $this->afficheAllAdherent();
					$main.= $this->afficheModifRetourEchec();
					break;
				case 'retourformsuppdocsucces':
					$main = $this->suppDoc();
					$main.= $this->afficheSuppDocRetourSucces();
					break;
				case 'retourformsuppdocechec':
					$main = $this->suppDoc();
					$main.= $this->afficheSuppRetourEchec();
					break;
				case 'retourformadddocsucces':
					$main = $this->ajoutDoc();
					$main.= $this->afficheAddDocRetourSucces();
					break;
				case 'retourformadddocechec':
					$main = $this->ajoutDoc();
					$main.= $this->afficheSuppRetourEchec();
					break;
				case 'formsupp':
					$main = $this->suppAdherent();
					break;
				case 'formmodif':
					$main = $this->modifAdherent();
					break;
				case 'formajoutdoc':
					$main = $this->ajoutDoc();
					break;
				case 'formsuppdoc':
					$main = $this->suppDoc();
					break;
				case 'formafficheadherent':
					$main = $this->afficheAllAdherent();
					break;

		}
		$html ="<!DOCTYPE html>
				<html>
				<head> 
					<title>Mediatheque</title>
					<meta charset='UTF-8'>
					<meta name='viewport' content='initial-scale=1, maximum-scale=1'>
					<link rel='stylesheet' type='text/css' href='/$this->racine/css/css/style.css'>
				</head>
				<body>";

		$html.= "<header>".$header."</header>";
		$html.= "<section id='content'><nav>".$nav."</nav>";
		$html.= "<section>".$main."</section></section>";
		$html.= "</body></html>";
		echo $html;
	}


	function afficheHead(){
		return "<h1>Mediatheque</h1>";
	}
	
 
	function afficheNav($param){
		$n="<ul class='navbar'>";
		switch ($param) {
			case 'formetat':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'retouretat':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'emprunter':
				$n.="<li class='active'><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";
				break;
			case 'restituer':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'retourformajoutsucces':
			case 'retourformajoutechec':
			case 'formajout':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'formafficheadherent':
			case 'retourformmodifsucces':
			case 'retourformmodifechec':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'retourformsuppsucces':
			case 'retourformsuppechec':
			case 'formsupp':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'retourformadddocsucces':
			case 'retourformadddocechec':
			case 'formajoutdoc':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";

				break;
			case 'retourformsuppdocsucces':
			case 'retourformsuppdocechec':
			case 'formsuppdoc':
				$n.="<li><a href='/$this->racine/index.php/MediathequeControler/emprunt'>Emprunt</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/restituer'>Restituer</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formetat'>Gestion des documents</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajout'>Ajouter un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formafficheadherent'>Modifier un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formsupp'>Supprimer un adhérant</a></li>
					<li><a href='/$this->racine/index.php/MediathequeControler/formajoutdoc'>Ajouter un document</a></li>
					<li class='active'><a href='/$this->racine/index.php/MediathequeControler/formsuppdoc'>Supprimer un document</a></li>";
				break;
		}
		$n.="</ul>";

		return $n;
		
	}
	
	/**
	*
	*	
	*	afficheEtat produit le code HTML du formulaire de saisie
	*	@author : Osman
	*
	**/
	function afficheEtat(){
	
		$f = "<h4>Enregistrer l'état d'un média dans la base de données :</h4>

		<article>
			<form method='post' action='/$this->racine/index.php/MediathequeControler/etat'>
						
			<label>Référence du média :</label> <input type='number' name='idmedia'>

			<select name='etat'>

				<option value='reparation'>En réparation</option>
				<option value='perdu'>Perdu</option>		
			</select>

			<input type='submit' class='bouton bouton-bleu' value='Enregistrer'>
		    </form>
		</article>
		<hr>
		<h4>Enregistrer une reservation pour un adhérant:</h4>

		<article>
			<form method='post' action='/$this->racine/index.php/MediathequeControler/reserve'>
						
			<label>Référence du média :</label> <input type='number' name='idmedia'>
			<label>Référence de l'adhérant :</label> <input type='number' name='id_adherant'>

			<input type='submit' class='bouton bouton-bleu' value='Enregistrer'>
		    </form>
		</article>
		";
		return $f;
	}

	/**
	*
	*	
	*	La fonction Afficheremprunt affiche le formulaire de saisie pour
	*	enprunter un document @author Anthony
	*
	**/
	function afficheEmprunt(){
		$f = "
		<h4>Emprunter</h4>
		<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/emprunt/'>
		<p>Id de l'adherent:</p><input type='text' name='id' id='id' placeholder='Entrez id de ladherent ici'>
		";

		for ($i=0; $i <6; $i++) { 
			$f.="<p>Reference du livre:</p><input type='text' name='reference".$i."' id='reference".$i."' placeholder='Entrez la reference ici'>";

		}

		
	
		$f.="<br><input class='bouton bouton-bleu' type='submit' value='Emprunter'>
		</form>";
		
			if ((isset($this->sortie)) && (isset($this->liste))) {
				$adh = $this->sortie;
				$mydate= $this->date_retour;
				if (!empty($adh)) {
				if (!empty($mydate)) {
				$f.="<p>L'adherent ".$adh->nom." ".$adh->prenom." a emprunter le(s) documents suivant(s):";
				foreach ($this->liste as $key => $value) {
					$f.="<br>-Reference: ".$value->ref." Type: ".$value->type." Titre: ".$value->nom;
				}
					$f.="<br><p>Vous avez emprunter : ".count($this->liste)." documents</p>";
					$f.="<br><p>Vous devez rendre ces documents le: ".$mydate->date_retour;
							
				}
				else{
					$f.="<p class='alert-box-danger'>Ce document est reserver ou emprunter</p>";
				}

				}
				else{
					$f.="<p class='alert-box-danger'> Id adherents non trouvé</p>";
				}
		}
		//}
		

	
		return $f;
	}
	/**
	*
	*	
	*	La fonction afficheRestituer affiche le formulaire de saisie pour
	*	enprunter un document @author Anthony
	*
	**/
	function afficheRestituer(){
		$f = "
		<h4>Restituer</h4>
		<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/restituer/'>";
		for ($i=0; $i <6; $i++) { 
			$f.="<p>Reference du livre</p><input type='text' name='reference".$i."' id='reference".$i."' placeholder='Entrez la reference ici'>"

		;}
		$f.="<br><input class='bouton bouton-bleu' type='submit' value='Restituer'>
		</form>";

		if ((isset($this->sortie)) && (isset($this->liste))) {
			$adh = $this->sortie;
			if (!empty($adh)) {
			$f.="<p>".$adh->nom." ".$adh->prenom." a restituer les documents suivants:";
				foreach ($this->liste as $key => $value) {
					$f.="<br>-".$value->type." : ".$value->nom;
				}
			
		}
		else{
			$f.="<p class='alert-box-danger'>Documents non trouve</p>";
		}
		$mesdocs1 = $this->mesdocuments;
		if (!empty($mesdocs1)) {
			
			foreach ($mesdocs1 as $key => $value) {
				$f.= "<ul>";
					$f.="<li><h2>Il vous reste a restituer:</h2></li>";
					$f.="<li><h2>".$value->nom."</h2></li>";
					$f.="<li><h6>".$value->genre."</h6></li>";
					$f.="<li><h6>Date de sortie: ".$value->date_sortie."</h6></li>";
					$f.="<li><h6>Date de retour: ".$value->date_retour."</h6></li>";
				$f.= "</ul>";
			}
			$f.="<br><p>Il vous reste : ".count($mesdocs1)." documents a rendre </p>";

		}
		else{
			$f.="<li><h2 class='alert-box-success'>Vous n'avez plus rien a restituer</h2></li>";
		}
		
		$f.="<br><p>Vous avez rendu : ".count($this->liste)." documents </p>";
		}
		if (!empty($this->norefe)) {
			$f.="<p class='alert-box-danger'>Reference non trouve</p>";

		}
		return $f;
	}

	public static function afficheEtatRetour(){
		$info = "<article class='alert-box-success'>Etat du document modifié</article>";
		return $info;
	}

	public function afficheAjoutRetourSucces(){
		$info = "<article class='alert-box-success'>Ajout de l'adhérent avec succès<br>
		ID corespondant : $this->id</article>";
		return $info;
	}

	public static function 	afficheAjoutRetourEchec(){
		$info = "<article class='alert-box-danger'>Echec de l'ajout de l'adhérent</article>";
		return $info;
	}
	public static function afficheSuppRetourSucces(){
		$info = "<article class='alert-box-success'>Suppression de l'adhérent effectué avec succès</article>";
		return $info;
	}
	public static function 	afficheSuppRetourEchec(){
		$info = "<article class='alert-box-danger'>Echec de la suppression</article>";
		return $info;
	}
	public static function afficheModifRetourSucces(){
		$info = "<article class='alert-box-success'>Modification de l'adhérent effectué avec succès</article>";
		return $info;
	}
	public static function 	afficheModifRetourEchec(){
		$info = "<article class='alert-box-danger'>Echec de la modification de l'adhérent</article>";
		return $info;
	}
	public static function afficheSuppDocRetourSucces(){
		$info = "<article class='alert-box-success'>Suppression du document effectué avec succès</article>";
		return $info;
	}
	public static function afficheAddDocRetourSucces(){
		$info = "<article class='alert-box-success'>Ajout du document effectué avec succès</article>";
		return $info;
	}
	/**
	*
	*	
	*	ajoutAdherent produit le code HTML du formulaire de saisie
	*	pour ajouter un adherent 
	*       @author : Osman
	**/

	function ajoutAdherent() {
		$f = "

		<h4>Ajouter un adhérent :</h4>
	
		<div class='#'>
			<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/ajout'>
						
				<p>Nom  : <input type='text' name='nomadherent'></p>
				<p>Prénom  : <input type='text' name='prenomadherent'></p>

				<p><input type='submit' class='bouton bouton-bleu' value='Enregistrer'/></p>
		    	</form>
		</div>
		";
	
		return $f;
		

	}

	/**
	*
	*	
	*	suppAdherent produit le code HTML du formulaire de saisie
	*	pour supprimer un adherent
	*       @author : Osman
	**/


	function suppAdherent(){

		$f = "
		<h4>Supprimer un adhérent :</h4>
		<div class='#'>
			<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/supp'>
				<p>Entrer l'id de l'adhérent à supprimer : <input type='number' name='idadherent' value='' /></p>
				<p><input type='submit' class='bouton bouton-bleu' value='Supprimer'/></p>
		    </form>
		</div>
		";
	
		return $f;


	}
	

	function modifAdherent(){
		$complete = $this->id;
		$f = "
		<h4>Modifier les informations d'un adhérent :</h4>
	
		<div class='#'>
			<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/modif'>
				<input type='hidden' name='id' value='$complete->id'>
				<p>Nom  : <input type='text' name='nom' value='$complete->nom'></p>
				<p>Prenom  : <input type='text' name='prenom' value='$complete->prenom'></p>

				<p><input type='submit' class='bouton bouton-bleu' value='Mettre à jour'/></p>
		    	</form>
		</div>
		";
		return $f;
	}

	function afficheAllAdherent(){
				
		$array = $this->tab;
		$f = '<table>
				<tr>
					<td>Id</td>
					<td>Nom</td>
					<td>Prénom</td>
					<td>Modifier</td>
				</tr>
			';
		foreach ($array as $key => $value){
			$f .= "<tr>
				<td>".$value->id."</td>
				<td>".$value->nom."</td>
				<td>".$value->prenom."</td>
				<td><a href='formmodif?id=$value->id'>Modifier</a></td>
			</tr>";
		}
		$f .= '</table>';


		return $f;
	}

	

	
	
	function ajoutDoc() {
		$f = "

		<h4>Ajouter un document :</h4>
	
		<div class='ajoutDoc'>
			<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/ajoutdoc' enctype='multipart/form-data'>
						
				<p>Type  : <select name='typedoc'>
						<option value='nouveau'>Nouveau</option>";
		foreach ($this->tab as $key => $value) {
			$f.="<option value='".$value."' class='firstLetter'>".$value."</option>";
		}
		$f.="</select> ou 
			<input type='text' name='newtypedoc' placeholder='Nouveau type'>
			</p>

			<p>Genre  : <select name='genredoc'>
						<option value='nouveau'>Nouveau</option>";
		foreach ($this->doc as $key => $value) {
			$f.="<option value='".$value."' class='firstLetter'>".$value."</option>";
		}
		$f.="</select> ou <input type='text' name='newgenredoc' placeholder='Nouveau genre'></p>
			<p>Nom  : <input type='text' name='nomdoc'></p>
			<p>Descriptif : <textarea name='descriptifdoc'></textarea></p>
			<p>Auteur : <input type='text' name='auteurdoc' ></p>
			<p>Image : <input type='file' name='file' id='file'></p> 

			<p><input type='submit' class='bouton bouton-bleu' value='Enregistrer'/></p>
		    </form>
		</div>
		";
	
		return $f;	

	}


	function suppDoc(){

		$f = "

		<h4>Supprimer un document :</h4>
	
		<div class='#'>
			<form class='ajoutDoc' method='post' action='/$this->racine/index.php/MediathequeControler/suppdoc'>
				
				<p>Entrer l'id du document à supprimer : <input type='number' name='iddoc' value='' /></p>


				<p><input type='submit' class='#' value='Supprimer'/></p>
		    	</form>
		</div>
		";
	
		return $f;

	}


}
