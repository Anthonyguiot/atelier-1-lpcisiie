<?php
namespace utils;
class Connexion {
  
  private static $db_link ;
  private static $config_file = 'conf/config.ini';
  
  /* établie une connexion a la base et retourne une instance de PDO */
  /* 1) lire les paramètres de connexion depuis fichier ini */
  /*    (voir  la fonction parse_ini_file ) */
  /* 2) créer une instance de l'objet PDO avec le bon DSN  */
  /*    et la stocker dans $db_link */
  /* 3) en cas derreur récupérer l'exeption est terminer le programme */
  private static function connect() { 
    try {
      
      $config = parse_ini_file(self::$config_file );

      $dsn = "mysql:host=".$config['hostname'].";dbname=".$config['dbname'];
      $db = new \PDO($dsn , $config['user'], $config['password'], 
        array(\PDO::ERRMODE_EXCEPTION => true,
              \PDO::ATTR_PERSISTENT => true)
      );

    } catch(\PDOException $e) {
      echo "Erreur de connexion : $dsn (".$e->getMessage().")<br/>" ;
      exit;
    }
   
    $db->exec("SET CHARACTER SET utf8");
    self::$db_link = $db;
  }

  /* Retourne l'instance de connexion */
  /* 1) vérifier si la connexion existe (attribut $db_link) */
  /*    si n'existe pas le créer avec la methode connect */
  /* 2) retourner l'instance */
  public static function getConnexion() {
    /* Compléter ici */
    if (!self::$db_link) {
      self::connect();
    } 
    return self::$db_link;
    
  }
}