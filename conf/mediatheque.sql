-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2015 at 03:51 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mediatheque`
--

-- --------------------------------------------------------

--
-- Table structure for table `adherants`
--

CREATE TABLE IF NOT EXISTS `adherants` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `mdp` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `adherants`
--

INSERT INTO `adherants` (`id`, `nom`, `prenom`, `mdp`) VALUES
(1, 'Bar', 'Leni', '$2y$10$fjmgGQDlFVgTIMfV2PE/qukPzZOhJe71dtl5IIPWFZjf3qjNaqCQe'),
(2, 'Némard', 'Jean', '$2y$10$s0vDCrQKN.YHHeATcVjZYe2GZutB6gtjVhJjJyOTA.DraEvO5boky'),
(3, 'Bonneau', 'Jean', '$2y$10$eGf/SiaDTED.ne674scYRer/dJ/BE4l.22m9jSQ3KRPeEmK5bfBKe'),
(4, 'Don D''vello', 'Guy', '$2y$10$OT0.7vdA5Z3ePFPgzwUCDe2RGM4UVxRaq/GC.k0qLFeskuxoWakAW');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `ref` int(8) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `genre` varchar(30) NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 NOT NULL,
  `descriptif` text CHARACTER SET utf8 NOT NULL,
  `auteur` varchar(30) CHARACTER SET utf8 NOT NULL,
  `image` varchar(50) CHARACTER SET utf8 NOT NULL,
  UNIQUE KEY `ref` (`ref`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`ref`, `type`, `genre`, `nom`, `descriptif`, `auteur`, `image`) VALUES
(1, 'DVD', 'humour', 'La cage aux folles', 'Renato Baldi et Albin Mougeotte sont un couple homosexuel presque sans histoire, à part les caprices récurrents d''Albin. Ils possèdent un cabaret de travestis, La Cage Aux Folles, dont Albin est l’artiste principal, sous le pseudonyme de Zazà Napoli.\n\nRenato a eu jadis une brève liaison avec une femme, dont est résulté un fils, Laurent. Ce dernier rend visite à son père et lui apprend qu''il a l''intention de se marier avec sa petite amie, Andréa. Bien qu''un peu vexé d''apprendre que son fils est hétérosexuel, Renato s''accommode de la situation. Le problème est qu''Andréa est la fille du député Simon Charrier, membre éminent du parti ultraconservateur « Union pour l''ordre moral » et père de famille particulièrement sévère. Pour ne pas choquer sa future belle-famille, Laurent demande à son père de jouer le jeu de la famille respectable. Renato accepte à contrecœur : cela implique d''éloigner Albin le temps de la rencontre entre les deux familles, ce que l''intéressé risque de très mal prendre.', 'Édouard Molinaro', 'la_cage_aux_folles'),
(2, 'livre', 'jeunesse', 'L''âne trotro fait du vélo', 'A califourchon sur son splendide vélo rouge, l''âne Trotro multiplie les prises de risque. Sans la main droite, sans la main gauche, sans les deux mains… A force de défier les lois de l''équilibre, il ne tarde pas à découvrir la loi de la gravité…', 'Benedicte Guettier', 'l_ane_trotro_fait_du_velo'),
(3, 'livre', 'roman', 'Cinquante nuances de Grey', 'Romantique, liberateur et totalement addictif, ce roman vous obsedera, vous possedera et vous marquera a jamais.\n \nLorsqu''Anastasia Steele, etudiante en litterature, interviewe le richissime jeune chef d''entreprise Christian Grey, elle le trouve tres seduisant mais profondement intimidant. Convaincue que leur rencontre a ete desastreuse, elle tente de l''oublier - jusqu''a ce qu''il debarque dans le magasin ou elle travaille et l''invite a un rendez-vous en tete-a-tete. \n\nNaive et innocente, Ana ne se reconnait pas dans son desir pour cet homme. Quand il la previent de garder ses distances, cela ne fait que raviver son trouble. \n\nMais Grey est tourmente par des demons interieurs, et consume par le besoin de tout controler. Lorsqu''ils entament une liaison passionnee, Ana decouvre ses propres desirs, ainsi que les secrets obscurs que Grey tient a dissimuler aux regards indiscrets. ', 'E.L. James', 'cinquante_nuance_de_grey'),
(4, 'livre', 'apprentissage', 'Javascript et HTML pour les nuls', 'Pour comprendre enfin quelque chose à la micro-informatique !\n\nUn nouveau matériel ou un nouveau logiciel vient de débarquer dans votre vie et vous n''avez pas de temps à perdre pour en apprendre l''utilisation. Deux solutions s''offrent à vous, attendre un miracle, solution peu probable, ou faire confiance à votre Mégapoche qui vous donnera toutes les informations essentielles pour démarrer un apprentissage efficace dans la joie et la bonne humeur !\n\nApprenez à parler Web !\n\nGrâce à ce livre, HTML 4, le langage de base du Web, et JavaScript celui qui permet d''animer vos pages sont enfin à la portée du commun des mortels ! Apprenez à mettre en oeuvre les balises, les frames, la gestion des cookies, les images réactives et les rollovers en quelques heures, HTML 4 et JavaScript n''auront plus de secret pour vous !\n\nDÉCOUVREZ\n\nLes bases pour créer des pages Web\nLes CSS\nLes tableaux\nLes formulaires et les scripts\nXHTML\nJavaScript et les cookies\nLes pages Web interactives\nImages réactives et rollovers', 'Emily A. Vander Veer', 'HTML_et_JavaScript_pour_les_nuls'),
(5, 'CD', 'rap', 'D.C.U', '7ème album solo très attendu\r\n\r\nPorté par les singles OKLM, 3G & Tony Sosa, avec les participations évènement des stars internationales Future, Jeremih, ou encore Mavado, et avec un concert à Bercy et une tournée dans toute la France en ligne de mire, ce nouvel album de Booba est l’évènement rap de cette année 2015.', 'Booba', 'DCU'),
(6, 'CD', 'disco', 'The best of Village People', 'Le CD idéal pour vous rendre gai', 'Village People', 'the_best_of_village_people'),
(7, 'DVD', 'humour', 'La moustache', 'Un jour, pensant faire sourire votre femme et vos amis, vous rasez la moustache que vous portiez depuis dix ans. Personne ne le remarque ou, pire, chacun feint de ne l''avoir pas remarqué, et c''est vous qui souriez jaune. Tellement jaune que, bientôt, vous ne souriez plus du tout. Vous insistez, on vous assure que vous n''avez jamais eu de moustache. Deviendriez-vous fou ? Voudrait-on vous le faire croire ? Ou quelque chose, dans l''ordre du monde, se serait-il détraqué à vos dépens ? L''histoire, en tout cas, finit forcément très mal et, d''interprétations impossibles en fuite irraisonnée, ne vous laisse aucune porte de sortie', 'Emmanuel Carrère ', 'la_moustache');

-- --------------------------------------------------------

--
-- Table structure for table `sortie`
--

CREATE TABLE IF NOT EXISTS `sortie` (
  `id_sortie` int(8) NOT NULL AUTO_INCREMENT,
  `ref_doc` int(8) NOT NULL,
  `id_adherant` int(8) NOT NULL,
  `date_sortie` date NOT NULL,
  `date_retour` date NOT NULL,
  `etat` varchar(50) NOT NULL,
  UNIQUE KEY `id_sortie` (`id_sortie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sortie`
--

INSERT INTO `sortie` (`id_sortie`, `ref_doc`, `id_adherant`, `date_sortie`, `date_retour`, `etat`) VALUES
(1, 1, 1, '2015-11-04', '2015-11-19', 'emprunté'),
(2, 2, 1, '2015-11-04', '2015-11-19', 'emprunté'),
(3, 3, 1, '2015-11-04', '2015-11-19', 'emprunté'),
(4, 4, 2, '2015-11-04', '2015-11-19', 'emprunté'),
(5, 5, 2, '2015-11-04', '2015-11-19', 'emprunté');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
